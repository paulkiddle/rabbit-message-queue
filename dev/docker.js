import { spawn } from 'child_process';
import { Transform } from 'stream';

const levels = {
	debug: 0,
	info: 1,
	log: 2,
	warn: 3,
	error: 4
}

class RabbitLogs extends Transform {
	constructor(logLevel = 'warn'){
		const p = new Promise((resolve, reject) => {
			super({
				transform(chunk, encoding, callback){
					const line = String(chunk).match(/[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3} \[(?<level>[^\]]+)\] <[^>]+> (?<message>.*)/)?.groups;
					if(!(levels[line?.level]) || levels[line.level] >= levels[logLevel]) {
						this.push(chunk);
					}
					if(line?.message && line.message.startsWith('Server startup complete')) {
						resolve();
					}
					callback();
				}
			});
			this.once('end', reject);
		});
		this.started = p;
	}
}

const SIGINT = 'SIGINT';
const SAFE_EXIT = 130;

class Rabbit{
	constructor(options){
		Object.assign(this, options);
	}

	get url(){
		return `amqp://${this.host}:${this.port}`;
	}

	toString(){
		return this.url;
	}
}

export default async function* rabbitDocker({port=5672, name='rabbitmq', logs=process.stderr, host='localhost', logLevel}={}, args=[]) {
	const docker = spawn(
		'docker',
		['run', '--rm', '--name', name, '-p', '5672:' + port, ...args, 'rabbitmq:3-management']
	);


	docker.stderr.pipe(logs);
	const rl = new RabbitLogs(logLevel);
	docker.stdout.pipe(rl).pipe(logs);

	const exit = new Promise((resolve, reject) => {
		docker.once('exit', (code) => {
			if(code && code !== SAFE_EXIT) {
				reject(code);
			} else {
				resolve(code);
			}
		});
	});

	try {
		await Promise.race([
			rl.started,
			exit
		]);

		yield new Rabbit({
			host,
			port,
			exit
		});
	} finally {
		docker.kill(SIGINT);
		await exit;
	}
}

// for await(const rabbit of rabbitDocker()) {
// 	console.log('rabbit at ' + rabbit);
// 	await new Promise(re=>setTimeout(re, 10000));
// }
