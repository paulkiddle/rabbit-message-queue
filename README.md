# Rabbit Message Queue

An implementation of [abstract-message-queue](https://www.npmjs.com/package/abstract-message-queue) for a [RabbitMQ](https://www.rabbitmq.com/) backend.

In short, creates an async iterator that acts as a worker queue.

Uses the `for await of` pattern for resource cleanup.

To use:

```javascript
import RabbitMessageQueue from 'rabbit-message-queue';

for await(const queue of RabbitMessageQueue.connect('amqp://://localhost:5672', 'my_queue')) {
	await myApplicationThatUsesAbstractMessageQueue(queue);
}
```

For more details on how to use the `queue` instance, see the
[abstract-message-queue README](https://gitlab.com/paulkiddle/abstract-message-queue).

## Dependencies
 - abstract-message-queue: ^1.0.0
 - rabbit.js: ^0.4.4
<a name="module_rabbit-message-queue"></a>

## rabbit-message-queue
Rabbit Message Queue

**See**: [default](#module_rabbit-message-queue.default)  

* [rabbit-message-queue](#module_rabbit-message-queue)
    * [.default](#module_rabbit-message-queue.default)
    * [.connect(url, table)](#module_rabbit-message-queue.connect)
    * [.sender(url, table)](#module_rabbit-message-queue.sender)

<a name="module_rabbit-message-queue.default"></a>

### rabbit-message-queue.default
**Kind**: static property of [<code>rabbit-message-queue</code>](#module_rabbit-message-queue)  
<a name="module_rabbit-message-queue.connect"></a>

### rabbit-message-queue.connect(url, table)
Connect to rabbit and yield an instance of the message queue

**Kind**: static method of [<code>rabbit-message-queue</code>](#module_rabbit-message-queue)  

| Param | Type | Description |
| --- | --- | --- |
| url | <code>string</code> | The URL of the rabbit instance to connect to |
| table | <code>string</code> | The queue name to connect to |

<a name="module_rabbit-message-queue.sender"></a>

### rabbit-message-queue.sender(url, table)
Connect to rabbit and yield a function for adding a message to the queue

**Kind**: static method of [<code>rabbit-message-queue</code>](#module_rabbit-message-queue)  

| Param | Type | Description |
| --- | --- | --- |
| url | <code>string</code> | The URL of the rabbit instance to connect to |
| table | <code>string</code> | The queue name to connect to |

