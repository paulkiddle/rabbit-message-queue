import MessageQueue from 'abstract-message-queue';
import rabbit, { sender } from './rabbit.js';

/**
 * Rabbit Message Queue
 * @module rabbit-message-queue
 * @see {@link module:rabbit-message-queue.default}
 */

/**
 * @name module:rabbit-message-queue.default
 */
export default class RabbitMessageQueue extends MessageQueue {
	#worker;
	#push;
	#ack;

	constructor({ worker, push }){
		let message;
		super({
			next: async()=>{
				const { value, done } = await this.#worker.next();
				if(done){
					throw new Error('The worker finished');
				}
				message = JSON.parse(value);
				return {
					message
				};
			},
			retry:()=>{
				this.send(message);
			},
			delete: ()=>{
				this.#ack();
			}
		});

		this.#ack = worker.ack.bind(worker);
		this.#worker = worker[Symbol.asyncIterator]();
		this.#push = writer(push);
	}

	/**
	 * Connect to rabbit and yield an instance of the message queue
	 * @param {string} url The URL of the rabbit instance to connect to
	 * @param {string} table The queue name to connect to
	 */
	static async *connect(url, table){
		for await(const { worker, push } of rabbit(url, table)) {
			yield new RabbitMessageQueue({ worker, push });
		}
	}

	/**
	 * Connect to rabbit and yield a function for adding a message to the queue
	 * @param {string} url The URL of the rabbit instance to connect to
	 * @param {string} table The queue name to connect to
	 */
	static async *sender(url, table) {
		for await(const push of sender(url, table)) {
			yield writer(push);
		}
	}

	async send(msg){
		await this.#push(msg);
	}
}

function writer(push) {
	return msg => push.write(JSON.stringify(msg));
}
