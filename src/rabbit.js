import rabbit from 'rabbit.js';

function close(closable){
	return new Promise(resolve => {
		closable.on('close', resolve);
		closable.close();
	});
}

function connect(connectable, ...options) {
	return new Promise(resolve => {
		connectable.connect(...options, ()=>resolve(connectable));
	});
}

async function* context(url){
	const context = rabbit.createContext(url);

	try {
		await new Promise((resolve, reject)=>{
			context.on('ready', resolve);
			context.on('error', reject);
		});
		yield context;
	} finally {
		await close(context);
	}
}

async function* socket(context, type, options) {
	const socket = context.socket(type, options);

	try {
		yield socket;
	} finally {
		await close(socket);
	}
}

async function* all(...iterators) {
	const out = await Promise.all(iterators.map(i=>i.next()));

	try {
		yield out.map(o=>o.value);
	} finally {
		await Promise.all(iterators.map(i=>i.return()));
	}
}


async function * getConnectedSocket(context, type, queue, options) {
	for await(const s of socket(context, type, options)) {
		await connect(s, queue);
		yield s;
	}
}

async function* workpair(context, table, options){
	for await(const [push, worker] of all(
		getConnectedSocket(context, 'PUSH', table, options),
		getConnectedSocket(context, 'WORKER', table, options),
	)) {
		yield { push, worker };
	}
}

export default async function * q(url, table){
	for await(const c of context(url)) {
		yield* workpair(c, table);
	}
}

export async function * sender(url, table) {
	for await(const c of context(url)) {
		yield* getConnectedSocket(c, 'PUSH', table);
	}
}
