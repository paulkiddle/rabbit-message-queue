import { assertQueue } from 'abstract-message-queue';
import rabbitDocker from '../dev/docker.js';
import RabbitMessageQueue from './index.js';
import { jest } from '@jest/globals';

test('Rabbit', async()=>{
	jest.setTimeout(15000);
	for await(const rabbit of rabbitDocker({ logLevel: 'warn' })) {
		jest.setTimeout(5000);
		for await(const queue of RabbitMessageQueue.connect(rabbit.url, 'table')){
			await expect(assertQueue(queue, ()=>new Promise(resolve=>setTimeout(resolve, 10)))).resolves.not.toThrowError();
		}
		for await(const send of RabbitMessageQueue.sender(rabbit.url, 'table')) {
			expect(send).toBeInstanceOf(Function);
		}
	}
});
