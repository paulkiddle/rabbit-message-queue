# Get main readme
cat README.md | awk '$0=="## Dependencies" {exit} {print}' > README.tmp

# Generate dependency list
echo "## Dependencies" >> README.tmp
node -e "console.log(Object.entries(require('./package.json').dependencies||{}).map(e=>' - '+e.join(': ')).join('\n') || 'None')" >> README.tmp

# Generate API docs
./node_modules/.bin/jsdoc2md src/*.js  >> README.tmp

mv README.tmp README.md
